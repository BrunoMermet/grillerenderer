import typing


class GrilleRenderer:
    """
    Classe permettant de transformer en chaîne de caractères un tableau à 2 dimensions.
    """
    GAUCHE = "gauche"
    DROITE = "droite"
    CENTRE = "centre"

    def __init__(self, grille, largeur_case: int = None, align: str = CENTRE,
        nb_lignes_titre: int = 0, extracteur: typing.Callable[[typing.Any], str] = None):
        """
        Constructeur
        :param grille:  le tableau à 2 dimensions à afficher
        :param largeur_case: la largeur de chaque case. Si non fournie, on calcule la largeur max nécessaire.
        :param align: : peut valoir GAUCHE, DROITE OU CENTRE (valeur par défaut) pour déterminer comment le contenu d'une
        case est disposé
        :param nb_lignes_titre: nombre de lignes de titre (0 par défaut). Les lignes de titres sont séparées des autres par
        une ligne double
        :param extracteur: la fonction utilisée pour convertir le contenu du chaque case en chaîne de caractères (str par
        défaut)
        :return: la chaîne représentant la grille (avec retour chariot final)
        """
        self.grille = grille
        self.hauteur: int = len(grille)
        self.largeur: int = len(grille[0])
        self.align = align
        self.nb_lignes_titre = nb_lignes_titre
        if extracteur is None:
            self.extracteur = str
        else:
            self.extracteur = extracteur
        self.largeur_case = largeur_case


    def __determiner_largeur_max(self):
        largeur_max: int = 0
        for ligne in self.grille:
            for case in ligne:
                largeur_case = len(self.extracteur(case))
                if largeur_case > largeur_max:
                    largeur_max = largeur_case
        return largeur_max


    def __ligne_haut(self) -> str:
        retour = "\N{BOX DRAWINGS LIGHT DOWN AND RIGHT}"
        for _ in range(self.largeur - 1):
            retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
            retour += "\N{BOX DRAWINGS LIGHT DOWN AND HORIZONTAL}"
        retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
        retour += "\N{BOX DRAWINGS LIGHT DOWN AND LEFT}"
        return retour


    def __ligne_intermediaire(self) -> str:
        retour = "\N{BOX DRAWINGS LIGHT VERTICAL AND RIGHT}"
        for _ in range(self.largeur - 1):
            retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
            retour += "\N{BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL}"
        retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
        retour += "\N{BOX DRAWINGS LIGHT VERTICAL AND LEFT}"
        return retour


    def __adapter(self, texte: str):
        texte = self.extracteur(texte)
        if self.largeur_case > len(texte):
            if self.align == "droite":
                en_tete = " " * (self.largeur_case - len(texte))
                texte = en_tete + texte
            elif self.align == "gauche":
                fin = " " * (self.largeur_case - len(texte))
                texte = texte + fin
            else:
                en_tete = " " * ((self.largeur_case - len(texte)) // 2)
                fin = " " * (self.largeur_case - len(texte) - len(en_tete))
                texte = en_tete + texte + fin
        return texte


    def __ligne_donnee(self, num_ligne: int) -> str:
        retour = "\N{BOX DRAWINGS LIGHT VERTICAL}"
        for num_colonne in range(self.largeur - 1):
            rep_donnee = str(self.grille[num_ligne][num_colonne])
            rep_donnee = self.__adapter(rep_donnee)
            retour += rep_donnee
            retour += "\N{BOX DRAWINGS LIGHT VERTICAL}"
        rep_donnee = str(self.grille[num_ligne][self.largeur - 1])
        rep_donnee = self.__adapter(rep_donnee)
        retour += rep_donnee
        retour += "\N{BOX DRAWINGS LIGHT VERTICAL}"
        return retour



    def __ligne_fin_titre(self) -> str:
        retour = "\N{BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE}"
        for _ in range(self.largeur - 1):
            retour += "\N{BOX DRAWINGS DOUBLE HORIZONTAL}" * self.largeur_case
            retour += "\N{BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE}"
        retour += "\N{BOX DRAWINGS DOUBLE HORIZONTAL}" * self.largeur_case
        retour += "\N{BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE}"
        return retour


    def __ligne_bas(self) -> str:
        retour = "\N{BOX DRAWINGS LIGHT UP AND RIGHT}"
        for _ in range(self.largeur - 1):
            retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
            retour += "\N{BOX DRAWINGS LIGHT UP AND HORIZONTAL}"
        retour += "\N{BOX DRAWINGS LIGHT HORIZONTAL}" * self.largeur_case
        retour += "\N{BOX DRAWINGS LIGHT UP AND LEFT}"
        return retour

    def __str__(self) -> str:
        if self.largeur_case is None:
            self.largeur_case = self.__determiner_largeur_max()
        retour: str = self.__ligne_haut()
        retour += "\n"
        for num_ligne in range(self.hauteur):
            retour += self.__ligne_donnee(num_ligne)
            retour += "\n"
            if num_ligne < self.hauteur - 1:
                if num_ligne == self.nb_lignes_titre - 1:
                    retour += self.__ligne_fin_titre()
                else:
                    retour += self.__ligne_intermediaire()
                retour += "\n"
        retour += self.__ligne_bas()
        retour += "\n"
        return retour


    def with_largeur_case(self, largeur_case: int):
        """
        Pour préciser une largeur spécifique à chaque case
        :param largeur_case: la largeur de chaque case
        :return: le GrilleRenderer modifié
        """
        self.largeur_case = largeur_case
        return self


    def with_alignement(self, alignement : str):
        """
        Pour préciser la disposition du contenu de chaque case
        :param largeur_case: peut valoir GAUCHE, DROITE, ou CENTRE
        :return: le GrilleRenderer modifié
        """
        self.align = alignement
        return self


    def with_extracteur(self, extracteur: typing.Callable[[typing.Any], str]):
        """
        Pour préciser comment chaque case est transformée en chaîne de caractères
        :param largeur_case: la fonction contenu d'une case -> str
        :return: le GrilleRenderer modifié
        """
        self.extracteur = extracteur
        return self


    def with_nb_lignes_titre(self, nb_lignes_titre: int):
        """
        Pour préciser le nombre de lignes de titre
        :param largeur_case: le nombre de lignes de titre
        :return: le GrilleRenderer modifié
        """
        self.nb_lignes_titre = nb_lignes_titre
        return self
