# Grille Renderer

## I. Présentation générale
Ce programme a pour but d'obtenir facile une représentation texte
d'un tableau en 2 dimensions, en utilisant les caractères utf-8
dédiés.

## II. Paramétrages possibles
Voici les différents paramètres qu'il est possible de fixer :
- la **largeur de chaque case** : par défaut, c'est la largeur max
nécessaire pour représenter chacune des cases qui est prise en compte ;

- l'**alignement du contenu des cases** : si la largeur d'une case est
plus grande que la largeur requise, on peut choisir de positionner
le texte à gauche, à droite, ou de façon centrée (valeur par défaut) ;

- le **nombre de lignes de titre** : si on souhaite séparer des lignes de
titre du reste du tableau, on peut préciser le nombre de ces lignes
(valeur par défaut : 0). Ces ligne seront séparées des autres par une
ligne double ;

- l'**extracteur** : c'est la fonction de conversion du contenu d'une case en chaîne de caractères.
Pa défaut, c'est la fonction `str()` qui est utilisée.

## III. Utilisation
Les différents paramètres peuvent être passés au constructeur. Seul
le tableau à 2 dimensions est un paramètre requis. Il est possible de
modifier un *GrilleRenderer avec les méthodes `with_nom_paramètre`
pour modifier la valeur d'un paramètre

## IV. Exemples
Dans les exemples qui suivent, on suppose que la variable `grille` a
été initialisée ainsi :
```python
grille = [['ab', 'cde', 'f'], ['c', 'd', 'hi'], ['efgh', 'zz', 't']]
```

### 1. Génération par défaut
```python
print(GrilleRenderer(grille))
```

Sortie :
```
┌────┬────┬────┐
│ ab │cde │ f  │
├────┼────┼────┤
│ c  │ d  │ hi │
├────┼────┼────┤
│efgh│ zz │ t  │
└────┴────┴────┘
```

### 2. En utilisant les paramètres du constructeur
```python
print(GrilleRenderer(grille, largeur_case = 6, align=GrilleRenderer.GAUCHE))
```

Sortie :
```
┌──────┬──────┬──────┐
│ab    │cde   │f     │
├──────┼──────┼──────┤
│c     │d     │hi    │
├──────┼──────┼──────┤
│efgh  │zz    │t     │
└──────┴──────┴──────┘
```

```python
print(GrilleRenderer(grille, nb_lignes_titre=1, align=GrilleRenderer.DROITE, extraction=lambda texte: str(len(texte)*3)))
```
Sortie :
```
┌──┬──┬──┐
│ 6│ 9│ 3│
╞══╪══╪══╡
│ 3│ 3│ 6│
├──┼──┼──┤
│12│ 6│ 3│
└──┴──┴──┘
```

### 3. En utilisant les méthodes `with...`
```python
print(GrilleRenderer(grille).with_largeur_case(6)
                            .with_alignement(GrilleRenderer.GAUCHE)
     )
```

Sortie :
```
┌──────┬──────┬──────┐
│ab    │cde   │f     │
├──────┼──────┼──────┤
│c     │d     │hi    │
├──────┼──────┼──────┤
│efgh  │zz    │t     │
└──────┴──────┴──────┘
```

```python
print(GrilleRenderer(grille).with_nb_lignes_titre(1)
                            .with_alignement(GrilleRenderer.DROITE)
                            .with_extracteur(lambda texte: str(len(texte)*3))
     )
```
Sortie :
```
┌──┬──┬──┐
│ 6│ 9│ 3│
╞══╪══╪══╡
│ 3│ 3│ 6│
├──┼──┼──┤
│12│ 6│ 3│
└──┴──┴──┘
```
